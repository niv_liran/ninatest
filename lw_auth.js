var crypto = require('crypto');
var lw_users = require('./lw_users');
var lw_env = require('./lw_general');
var mysql = require('mysql');
var connection = mysql.createConnection({host:lw_env.SQLhost,user:lw_env.SQLuser,password:lw_env.SQLpass,database:lw_env.DBName});
connection.connect();

var url = require('url'),
	http = require('http'),
	qs = require('querystring'),
	request = require('request');

	var request = require('request');	
	var start = new Date();

/*exports.ValidateRequestQS = function(req, res, callback) {
		//TD - validations on clientid and hash
		var QS = url.parse(req.url,true).query;
		 
		var checkhash = QS.xhash; // Get incoming hash
		var client_id = QS.clientid;
		var inhash = req.originalUrl;
		var inhash = inhash.substring(1, inhash.indexOf('&xhash')); // Get original URL
  
		console.log('req queryObject ' + QS);
		console.log('req client_id ' + client_id);
		console.log('req  hash to calculate' + inhash);
		console.log('req incoming hash' + checkhash);
		
		var query = client.query('SELECT * from GetPrivateKey(\'' + client_id + '\')');

		//Push result into res1
		query.on('row', function(row) {
			if (row['result_code'] == 0)
			{
				var private_key = row['p_private_key'];
				var server_side = crypto.createHmac('sha256', private_key);
				server_side.update(inhash);
				var testhash = server_side.digest('hex');
				console.log('calculated hash ' + testhash);
				
				
				if (checkhash == testhash) //if locally generated hash equals to incoming hash, trust.
				{
						callback(0, 'OK');
				}
				else //don't bother calling the callback
				{
						callback(-1, 'EPIC FAIL');
				};
			}
			else
			{
				callback(-1, 'Invalid client ID');
			}
		});
};
*/
exports.ValidateRequest = function(req, res, callback) {
		
		var Secret, APIToken, DeviceOS, DeviceOSVersion, Locale, Platform, PushRegID, DeviceResolution, DeviceDensity;
		
		var subscriber_id = -1;
		var subscription_id = 0;
		var plan_id = 0;
	
		Secret = req.header('X-Nina-Secret');
		APIToken = req.header('X-Nina-API-Token');
		DeviceOS = req.header('X-Nina-Device-OS');
		DeviceOSVersion = req.header('X-Nina-Device-OS-Version');
		Locale = req.header('X-Nina-Locale');
		Platform  = req.header('X-Nina-Platform');
		PushRegID = req.header('X-Nina-Push-Reg-ID');
		DeviceResolution = req.header('X-Nina-Device-Resolution');
		DeviceDensity = req.header('X-Nina-Device-Density');
		  
		if (Secret == 'Nin@h@zGr8c0ff33!')
		{
			var query = connection.query('SELECT sb.subscriber_id, coalesce(subscription_id, 0) as subscription_id, coalesce(plan_id, 0) as plan_id, coalesce(subscription_status, 0) as subscription_status from subscribers sb left join subscriptions sp on sb.subscriber_id = sp.subscriber_id and subscription_status = 1 and valid_from <= now() and valid_until >= now() where external_id = \'' + APIToken + '\'');
			
			query.on('result', function(row) {
				subscriber_id = row['subscriber_id'];
				subscription_id = row['subscription_id'];
				plan_id = row['plan_id'];
				console.log(JSON.stringify(row));
			});
			
			//Push result into res1
			query.on('end', function(result) {
				connection.query('INSERT INTO ninatest.subscriber_actions (url_called, api_secret, subscriber_id, external_id, external_push_id, device_os, device_os_version, device_locale, device_platform, device_resolution, device_density, remote_ip, user_agent, created_at) ' +
							     'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())', 
								 [req.originalUrl, Secret, subscriber_id, APIToken, PushRegID, DeviceOS, DeviceOSVersion, Locale, Platform, DeviceResolution, DeviceDensity, req.socket.remoteAddress, req.headers['user-agent']]);

				if (subscriber_id != -1)
				{
					console.log('Subscriber id = ' + subscriber_id + ' subscription id = ' + subscription_id + ' planid = ' + plan_id);
					callback(0, 'User Exists', subscriber_id, plan_id);
				}
				else
				{
					lw_users.SilentRegisterDeviceID(req, function(result_code, result_text, subscriber_id) {
						if (result_code == 0)
						{
							callback(0, 'User created', subscriber_id, plan_id);
						}
						else
						{
							callback(result_code, 'Silent register failed! Error: ' + result_text, -1, 0);
						};
					});
				};
			});
		}
		else
		{
			callback(-1, 'Invalid parameter sent');
		};
};





