var lw_auth = require('./lw_auth');
var port = process.env.PORT || 5000;
var sys = require ('sys'),
	url = require('url'),
	http = require('http'),
	qs = require('querystring');
	
var start = new Date();

var lw_env = require('./lw_general');
var mysql = require('mysql');
var connection = mysql.createConnection({host:lw_env.SQLhost,user:lw_env.SQLuser,password:lw_env.SQLpass,database:lw_env.DBName});
connection.connect();

exports.GetBilling = function(req, res) {
// GET a billing by ID
};

exports.ChargeSubscriber = function(req, res) {
// POST
// Charges a subscriber 
// Input subscriberID, subscription, amount (Although amount can be taken from plan).
};

exports.CreateBilling = function(req, res) {
	// POST a billing by ID
	console.log('***Entered create Billing BODY = ' + JSON.stringify(req.body));
	lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{
			CreateBillingFunc(req.body.subscription_id, 
							  req.body.payment_method_id, 
							  req.body.billing_amount, 
							  req.body.subscriber_id,
							  function(billing_id, result_text) {
									console.log('CALLBACK');
									if (billing_id > 0) {
											console.log('BACK! new ID is ' + billing_id);
											res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':JSON.stringify({billing_id: billing_id}).length, 'X-Nina-User-Plan-Id' : plan_id, 'X-Nina-Subscriber-Id' : subscriber_id}); 
											res.end(JSON.stringify({billing_id: billing_id}));
									}
									else
									{
										console.log('BACK WITH ERROR: ' + billing_id);
										res.writeHead(500, {'Content-Type': 'text/plain'});
										res.end();
									};
							  }
			);
		}
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		};
	});
};

CreateBillingFunc = function(subscription_id, payment_method_id, billing_amount, subscriber_id, callback) {
	var billing_id = -1;
	
	if ((subscription_id == null) || 
		(subscriber_id == null) || 
		(payment_method_id == null) || 
		(billing_amount == null) ||
		(subscription_id == '') || 
		(payment_method_id == '') || 
		(billing_amount == '') ||
		(subscriber_id == ''))
		{console.log('Create Billing validation error'); callback(-1, 'Billing params validation error');}
	else {
		//Insert billing	
		console.log('***CBF 2');
		var query1 = connection.query('INSERT INTO billings(subscriber_id, subscription_id, payment_method_id, billing_status, billing_amount, created_by, created_at, external_payment_reference) ' +
									  'VALUES (?, ?, ?, 0, ?, \'Rest API\', now(), null)', [subscriber_id, subscription_id, payment_method_id, billing_amount]);

		query1.on('error', function(error) {
			console.log('CBF error ' + error);
			callback(-1, 'Billing query error: ' + error)
		});
					
		console.log('***CBF 3');
		//Get latest sequence value
		query1.on('result', function(row) {
			billing_id = row.insertId;
			console.log('CBF new billing_id = ' + row.insertId);
			callback(billing_id, 'Success')
		});		
	};
};

exports.UpdateBilling = function(req, res) {
// PUT a billing by ID
};

exports.ChargeBilling = function(req, res) {
	ChargeBillingFunc(1, function callback(result_code, result_text) {
		if (result_code > 0)
		{
			console.log('ALL GOOD: CB ' + result_code);
			res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':(JSON.stringify({billing_result : result_code})).length}); 
			res.end(JSON.stringify({billing_result : result_code}));	
		}
		else
		{
			console.log('BACK WITH ERROR: CB ' + result_code);
			res.writeHead(500, {'Content-Type': 'text/plain'});
			res.end();
		};
	});
};

ChargeBillingFunc = function(billing_id, callback) {
	//TD - a LOT!

	//Billing status
	// 0 - Initiated
	// 1 - Received
	// 2 - Authorised
	// 3 - Refused
	
	//var respJ = [];
	//respJ.push();
	
	if ((billing_id == null) || (billing_id == ''))
		{callback(-1, 'BillingID is empty');};
	
	callback(2, 'Authorized');
	
};

exports.GetById = function(req, res){
  var queryObject = url.parse(req.url,true).query;
  var res1 = [];
  var id = req.params.id;
  if (id != null)
  { var query = client.query('SELECT * FROM billings where billing_id =' + id); }
  else
  {
		res.writeHead(400, {'Content-Type': 'text/plain'});
		res.end('Validation error');
  }
  
  //Push result into res1
  query.on('row', function(row) {res1.push(JSON.stringify(row));});
    
  query.on('end', function (result) {
      //If we have result return 200 and the subscriber id
	  if (res1.length > 0)
	  {
		  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':JSON.stringify(res1).length}); 		  
		  res.end(JSON.stringify(res1));
	  }
	  else
	  {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write('NOT FOUND ' + id);
		res.end();	
	  }
  }); 
};

exports.GetById = function(req, res){
  var queryObject = url.parse(req.url,true).query;
  var res1 = [];
  var id = req.params.id;
  if (id != null)
  { var query = client.query('SELECT * FROM billings where billing_id =' + id); }
  else
  {
		res.writeHead(400, {'Content-Type': 'text/plain'});
		res.end('Validation error');
  }
  
  //Push result into res1
  query.on('row', function(row) {res1.push(JSON.stringify(row));});
    
  query.on('end', function (result) {
      //If we have result return 200 and the subscriber id
	  if (res1.length > 0)
	  {
		  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':JSON.stringify(res1).length}); 		  
		  res.end(JSON.stringify(res1));
	  }
	  else
	  {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write('NOT FOUND ' + id);
		res.end();	
	  }
  }); 
};

exports.GetBySubscriber = function(req, res){
  var queryObject = url.parse(req.url,true).query;
  var res1 = [];
  var id = req.params.id;
  if (id != null)
  { var query = client.query('SELECT * FROM billings where subscriber_id =' + id); }
  else
  {
		res.writeHead(400, {'Content-Type': 'text/plain'});
		res.end('Validation error');
  }
  
  //Push result into res1
  query.on('row', function(row) {res1.push(JSON.stringify(row));});
    
  query.on('end', function (result) {
      //If we have result return 200 and the subscriber id
	  if (res1.length > 0)
	  {
		  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':JSON.stringify(res1).length}); 		  
		  res.end(JSON.stringify(res1));
	  }
	  else
	  {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write('NOT FOUND ' + id);
		res.end();	
	  }
  }); 
};