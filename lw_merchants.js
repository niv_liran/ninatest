var image_path = '/static/img';
var merchant_images = '/merchant';
var icon_path = '/icon';
var background_path = '/background';
var sys = require ('sys'),
	url = require('url'),
	http = require('http'),
	qs = require('querystring');
var lw_auth = require('./lw_auth');
var lw_env = require('./lw_general');
var mysql = require('mysql');
var connection = mysql.createConnection({host:lw_env.SQLhost,user:lw_env.SQLuser,password:lw_env.SQLpass,database:lw_env.DBName});
connection.connect();

var request = require('request');	
var start = new Date();

exports.GetNearbyMerchants = function(req, res) {
	var subscriber_id;

	lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{ 
		
		  // Get merchant list by input long/lat
		  var queryObject = url.parse(req.url,true).query;
		  var res1 = [];
		  var res2;
		  var vlat = queryObject.lat;
		  var vlong = queryObject.long;
		  var jsonp = queryObject.callback;
		  var i = 0;
		  
		  var DeviceOS = req.header('X-Nina-Device-OS');
		  var DeviceResolution = req.header('X-Nina-Device-Resolution');
		  var DeviceDensity = req.header('X-Nina-Device-Density');
	 
		  
		  if ((vlat !== undefined) && (vlong !== undefined) && (vlong != '') && (vlong != ''))
		  {							   
				var query = connection.query('SELECT merchant_id, merchant_name, concat(street, \' \' ,house_nr, \', \', city) as address, zipcode, country, lat, `long`, round(13000000  * ASIN(SQRT( POWER(SIN((? - abs(lat)) * pi()/180 / 2) ,2) + COS(? * pi()/180 ) * COS(abs(lat) * pi()/180) * POWER(SIN((? - `long`) * pi()/180 / 2), 2) ))) AS distance, icon_name ' +
											 ' FROM merchants order by distance limit 20', [vlat, vlat, vlong]);
				//Push result into res1
				  query.on('result', function(row) {
					var icon_name = lw_env.ConvertImageName(row['icon_name'], DeviceOS, DeviceDensity, DeviceResolution);
					
					icon_name = lw_env.StaticHost + image_path + merchant_images + '/' + row['merchant_id'] + icon_path + icon_name;
					
					res1.push(
					{merchant_id : row['merchant_id'],
					merchant_name : row['merchant_name'],
					address : row['address'],
					zipcode : row['zipcode'],
					country : row['country'],
					lat : row['lat'],
					long : row['long'],
					distance : row['distance'],
					icon_name : icon_name
					});
				  });
					
				  query.on('end', function (result) {
					  res2 = JSON.stringify({results : res1});
					  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':res2.length, 'X-Nina-User-Plan-Id' : plan_id, 'X-Nina-Subscriber-Id' : subscriber_id});
					  res.end(res2);
				  }); 
		  }
		  else
		  {
				console.log('3'); 
				console.log('Validation error'); 
				res.writeHead(400, {'Content-Type': 'text/plain'});
				res.end('Validation error');
		  }
		} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		}
	});

};

exports.GetById = function(req, res) {
  lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{ 
		  var queryObject = url.parse(req.url,true).query;
		  var res1 = [];
		  var res2;
		  var id = req.params.id;
		  
		  var DeviceOS = req.header('X-Nina-Device-OS');
		  var DeviceResolution = req.header('X-Nina-Device-Resolution');
		  var DeviceDensity = req.header('X-Nina-Device-Density');
	  
		  if (id != null)
		  {
				var query = connection.query('SELECT merchant_id, app_domain_id, merchant_name, concat(street, \' \' ,house_nr, \', \', city) as address, zipcode, country, lat, `long`, top_left_text, top_right_text, bottom_left_text, bottom_right_text, post_purchase_text, email, is_new, background_image_name, icon_name FROM merchants where merchant_id =' + id);
		  }
		  else
		  {
				res.writeHead(400, {'Content-Type': 'text/plain'});
				res.end('Validation error');
		  }
		  
		  //Push result into res1
		  query.on('result', function(row) {
			var icon_name = lw_env.ConvertImageName(row['icon_name'], DeviceOS, DeviceDensity, DeviceResolution);
			var background_image_name = lw_env.ConvertImageName(row['background_image_name'], DeviceOS, DeviceDensity, DeviceResolution);
			
			icon_name = lw_env.StaticHost + image_path + merchant_images + '/' + row['merchant_id'] + icon_path + icon_name;
			background_image_name = lw_env.StaticHost + image_path + merchant_images + '/' + row['merchant_id'] + background_path + background_image_name;		
			
			res1.push(
			{merchant_id : row['merchant_id'],
			merchant_name : row['merchant_name'],
			app_domain_id : row['app_domain_id'],
			address : row['address'],
			zipcode : row['zipcode'],
			country : row['country'],
			lat : row['lat'],
			long : row['long'],
			top_left_text : row['top_left_text'],
			top_right_text : row['top_right_text'],
			bottom_left_text : row['bottom_left_text'],
			bottom_right_text : row['bottom_right_text'],
			post_purchase_text : row['post_purchase_text'],
			email : row['email'],
			is_new : row['is_new'],
			icon_name : icon_name,
			background_image_name : background_image_name,
			});
		  });
			
		  query.on('end', function (result) {
			  //If we have result return 200 and the merchant id
			  if (res1.length > 0)
			  {
				  res2 = JSON.stringify({results : res1});
				  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':res2.length, 'X-Nina-User-Plan-Id' : plan_id, 'X-Nina-Subscriber-Id' : subscriber_id});
				  res.end(res2);
			  }
			  // Else return 404
			  else
			  {
				console.log('NOT FOUND ' + id);
				res.writeHead(404, {'Content-Type': 'text/plain'});
				res.write('NOT FOUND ' + id);
				res.end();	
			  }
		  }); 
		} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		}
	});
};

exports.InsertMerchant = function(req, res) {
// Insert new merchant, return his ID
};

