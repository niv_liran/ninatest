var lw_billings = require('./lw_billings');
var lw_auth = require('./lw_auth');

var port = process.env.PORT || 5000;
var sys = require ('sys'),
	url = require('url'),
	http = require('http'),
	qs = require('querystring');

var request = require('request');	
var lw_env = require('./lw_general');
var mysql = require('mysql');
var connection = mysql.createConnection({host:lw_env.SQLhost,user:lw_env.SQLuser,password:lw_env.SQLpass,database:lw_env.DBName});
connection.connect();


ValidatePurchase = function(subscriber_id, merchant_id, merchant_code, app_id, callback) {
  console.log('Validate purchase'); 
  var i = 0;
  	
  var query = connection.query('call ValidatePurchase(?, ?, ?, ?)', [subscriber_id, merchant_id, merchant_code, app_id]);
    
  //Push result into res1
  query.on('result', function(row) {
	i++;
	if (i==1)
	{
		console.log(JSON.stringify(row));  
		callback(row['result_text'], row['result_code'], row['p_subscription_id'], row['p_contract_id']);
	};
  });
};

exports.InsertNewPurchase = function(req, res) {
	
	lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{ 
			var respJ = [];
			var vsubscription_id;
			var vbilling_id;
			var vbilling_charge_result;
			
			console.log(req.body);
			if ((req.body.merchant_id == null) || 
				(req.body.merchant_code == null) || 
				(req.body.app_id == null) || 		
				(req.body.merchant_id == '') || 
				(req.body.merchant_code == '') || 
				(req.body.app_id == '')) 
				{console.log('INP Validation error'); res.writeHead(500, {'Content-Type': 'text/plain'}); res.end('Validation error');}
			else
			{
				console.log('*** 1');
			   
				//Check if purchase is valid
			   ValidatePurchase(subscriber_id, req.body.merchant_id, req.body.merchant_code, req.body.app_id, function(result_text, result_code, subscription_id, contract_id) {
					if (result_code == 0) //Purchase is valid, insert purchase
					{
						
						console.log('*** 2' + subscription_id + contract_id);
						var query1 = connection.query('INSERT INTO purchases(subscription_id, contract_id, created_at, upsell_flag, upsell_amount, units_purchased) VALUES (?, ?, now(), null, null, 1);',[subscription_id, contract_id]);

						query1.on('error', function(error) {
							console.log('*** 3.1');
							console.log(error);
							res.writeHead(500, {'Content-Type': 'text/plain'});
							res.end();
						});		
						
						query1.on('result', function(row) {
							console.log('*** 3');
							respJ.push({purchase_id: row.insertId}, {subscriber_id: subscriber_id}, {merchant_id: req.body.merchant_id}, {merchant_code: req.body.merchant_code},
										{app_id: req.body.app_id}, {subscription_id: subscription_id}, {contract_id: contract_id});
							res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':(JSON.stringify(respJ)).length, 'X-Nina-User-Plan-Id' : plan_id, 'X-Nina-Subscriber-Id' : subscriber_id}); 
							res.end(JSON.stringify(respJ));
						});
					}
					else //Shoot back the error
					{
						console.log(result_text);
						res.writeHead(500, {'Content-Type': 'text/plain'});
						res.write(result_text);
						res.end();
					};
			   });
			};
			} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		}
	});

};


exports.GetById = function(req, res){
	lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{ 
			  var queryObject = url.parse(req.url,true).query;
			  var res1 = [];
			  var id = req.params.id;
			  if (id != null)
			  { var query = connection.query('SELECT * FROM purchases where purchase_id =' + id); }
			  else
			  {
					res.writeHead(400, {'Content-Type': 'text/plain'});
					res.end('Validation error');
			  }
			  
			  //Push result into res1
			  query.on('result', function(row) {res1.push(row);});
				
			  query.on('end', function (result) {
				  //If we have result return 200 and the subscriber id
				  if (res1.length > 0)
				  {
					  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':JSON.stringify({results : res1}).length, 'X-Nina-User-Plan-Id' : plan_id, 'X-Nina-Subscriber-Id' : subscriber_id}); 		  
					  res.end(JSON.stringify({results : res1}));
				  }
				  else
				  {
					res.writeHead(404, {'Content-Type': 'text/plain'});
					res.write('NOT FOUND ' + id);
					res.end();	
				  }
			  }); 
  		} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		}
	});
};

exports.GetBySubscription = function(req, res){
  var queryObject = url.parse(req.url,true).query;
  var res1 = [];
  var id = req.params.id;
  if (id != null)
  { var query = client.query('SELECT * FROM purchases where subscription_id =' + id); }
  else
  {
		res.writeHead(400, {'Content-Type': 'text/plain'});
		res.end('Validation error');
  }
  
  //Push result into res1
  query.on('row', function(row) {res1.push(JSON.stringify(row));});
    
  query.on('end', function (result) {
      //If we have result return 200 and the subscriber id
	  if (res1.length > 0)
	  {
		  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':JSON.stringify(res1).length}); 		  
		  res.end(JSON.stringify(res1));
	  }
	  else
	  {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write('NOT FOUND ' + id);
		res.end();	
	  }
  }); 
};


