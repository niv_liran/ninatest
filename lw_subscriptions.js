var lw_billings = require('./lw_billings');
var lw_auth = require('./lw_auth');

var sys = require ('sys'),
	url = require('url'),
	http = require('http'),
	qs = require('querystring');

var request = require('request');	
var start = new Date();
var lw_auth = require('./lw_auth');
var lw_env = require('./lw_general');
var mysql = require('mysql');
var connection = mysql.createConnection({host:lw_env.SQLhost,user:lw_env.SQLuser,password:lw_env.SQLpass,database:lw_env.DBName});
connection.connect();

exports.Subscribe = function(req, res) {
// Subscription states:
// 0 - Initiated
// 1 - Active
// 2 - Expired

// POST
// Expects userID, PlanID and payment details.
// Calls payment function - TD!!
// If payment is successful, get billing id and create subscription
	var i = 0;
	lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{ 
			var respJ = [];
			var vsubscription_id;
			var vbilling_id;
			var vbilling_charge_result;

			if ((req.body.plan_id == null) || 
				(req.body.plan_id == '')) 
				{console.log('Validation error'); res.writeHead(500, {'Content-Type': 'text/plain'}); res.end('Validation error');}
			else
			{
			   //Initiate subscription
			   
			   // Insert new subscription
				var query1 = connection.query('call InitiateSubscription(?, ?)', [subscriber_id, req.body.plan_id]);

				query1.on('error', function(error) {
					console.log(error);
					res.writeHead(500, {'Content-Type': 'text/plain'});
					res.end();
				});
						
				//Get latest sequence value
				query1.on('result', function(row) {
					i++;
					if (row['result_code'] == 0) {
						vsubscription_id = row['p_subscription_id'];
						console.log('***3.1 S:'+ vsubscription_id);
						// Initiate billing record

						CreateBillingFunc(vsubscription_id, 1, 29.99, subscriber_id, function(vbilling_id, result_text) {
							console.log('CALLBACK BILLING');
							if (vbilling_id > 0) {
								console.log('BACK! new ID is ' + vbilling_id);
								ChargeBillingFunc(vbilling_id, function(result_code, result_text) {
									console.log('CALLBACK CHARGE');
									if (result_code == 2)
									{
										var query2 = connection.query('UPDATE subscriptions SET subscription_status = 1, valid_from = now(), valid_until = LAST_DAY(CURDATE()), billing_id = ? where subscription_id = ?',[vbilling_id, vsubscription_id]);
											
										query2.on('error', function(error) {
											console.log(error);
											res.writeHead(500, {'Content-Type': 'text/plain'});
											res.end();
										});
															
										query2.on('end', function(result) {
											console.log('***6');
											respJ.push({subscriber_id: subscriber_id}, {plan_id: req.body.plan_id}, {subscription_state : 1}, {billing_id : vbilling_id}, {billing_result : result_code},{subscription_id: vsubscription_id});
											res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':(JSON.stringify(respJ)).length, 'X-Nina-User-Plan-Id' : req.body.plan_id, 'X-Nina-Subscriber-Id' : subscriber_id}); 
											res.end(JSON.stringify(respJ));
										});
									}
									else
									{
										console.log('***7');
										respJ.push({subscription_state : 0, result_test : 'Billing failed with state ' + vbilling_charge_result});
										res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':(JSON.stringify(respJ)).length}); 
										res.end(JSON.stringify(respJ));
									}
								});				
							}
							else
							{
								console.log('BACK WITH ERROR1: ' + result_text);
								res.writeHead(500, {'Content-Type': 'text/plain'});
								res.end();
							};
						 });
				   }
				   else
				   {
						console.log('BACK WITH ERROR2: ' + row['result_text'] + i + ' ' + JSON.stringify(row));
						if (i==1)
						{
							res.writeHead(500, {'Content-Type': 'text/plain'});
							res.end(row['result_text']);
						}
				   };
				});
			};		
		} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		};
	});
};
