var sys = require ('sys'),
	url = require('url'),
	http = require('http'),
	qs = require('querystring');

var request = require('request');	
var start = new Date();

var lw_env = require('./lw_general');
var mysql = require('mysql');
var connection = mysql.createConnection({host:lw_env.SQLhost,user:lw_env.SQLuser,password:lw_env.SQLpass,database:lw_env.DBName});
connection.connect();

exports.SilentRegisterDeviceID = function(req, callback) {			
		var Secret, APIToken, DeviceOS, DeviceOSVersion, Locale, Platform, PushRegID;
		
		APIToken = req.header('X-Nina-API-Token')
		DeviceOS = req.header('X-Nina-Device-OS')
		DeviceOSVersion = req.header('X-Nina-Device-OS-Version')
		Locale = req.header('X-Nina-Locale')
		Platform  = req.header('X-Nina-Platform')
		PushRegID = req.header('X-Nina-Push-Reg-ID')
		
		// Insert subscriber
		var query1 = connection.query('INSERT INTO subscribers(registration_type, external_id, external_push_id, active_payment_method_id, device_os, device_os_version, device_locale, device_platform, created_at, created_by, updated_at, updated_by) VALUES (0, ?, ?, null, ?, ?, ?, ?, now(), ?, null, null)',
									  [APIToken, PushRegID, DeviceOS, DeviceOSVersion, Locale, Platform, 'RestAPI']);
		//var query1 = client.query(prepStatement);
		query1.on('error', function(error) {
			console.log(error);
			callback(-1, 'Insert error');
		});
			
		//Get latest sequence value
		query1.on('result', function(row) {
			callback(0, 'New subscriber created', row.insertId);
		});
};

exports.GetById = function(req, res){
  var queryObject = url.parse(req.url,true).query;
  var res1 = [];
  var id = req.params.id;
  console.log(queryObject);
  console.log('id:' + id); 
  
  if (id != null)
  {
		var query = connection.query('SELECT * FROM subscribers where subscriber_id =' + id);
  }
  else
  {
		console.log('3'); 
		console.log('Validation error'); 
		res.writeHead(400, {'Content-Type': 'text/plain'});
		res.end('Validation error');
  }
  
  //Push result into res1
  query.on('result', function(row) {
    res1.push(row);
  });
    
  query.on('end', function (result) {
      //If we have result return 200 and the subscriber id
	  if (res1.length > 0)
	  {
		  res.writeHead(200, {'content-type':'application/json; charset=utf8', 'content-length':JSON.stringify({results:res1}).length}); 		  
		  res.end(JSON.stringify({results:res1}));
	  }
	  // Else return 404
	  else
	  {
		console.log('NOT FOUND ' + id);
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write('NOT FOUND ' + id);
		res.end();	
	  }
  }); 
};

