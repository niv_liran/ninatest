var port = 3080;

var express = require('express');
var lw_users = require('./lw_users');
var lw_billings = require('./lw_billings');
var lw_subscriptions = require('./lw_subscriptions');
var lw_merchants = require('./lw_merchants');
var lw_purchases = require('./lw_purchases');
var lw_auth = require('./lw_auth');
var crypto = require('crypto');
var app = express.createServer(express.logger());
var fs = require('fs');

var webdir = '/web';

var url = require('url'),
	http = require('http'),
	qs = require('querystring');

app.configure(function(){
  //app.use(express.bodyParser());
  app.use(require('express').bodyParser());
  app.use(webdir,   express.static(__dirname+webdir));
  app.use(app.router);
});

app.get('/testmysql', function(req, res) {
	pool.getConnection(function(err, connection) {
		connection.query('SELECT 1 + 1 AS solution', function(err, rows, fields) {
			if (err) throw err;
			console.log('The solution is: ', rows[0].solution);
			res.write('Solution ' + rows[0].solution);
			connection.end();
		}); 
	});
  res.end('done');
});

app.get('/', function(req, res) {

  var queryObject = url.parse(req.url,true).query;

  var path = req.pathname;
  console.log('path = ' + path);
  if (path != null)
  {
	var img = fs.readFileSync(path);
	res.writeHead(200, {'Content-Type': 'image/gif' });
    res.end(img, 'binary');
  } 
  else
  {
		res.writeHead(400, {'Content-Type': 'text/plain'});
		res.end('Validation error');
  };
});

app.post('/users/subscribe', lw_subscriptions.Subscribe);
app.post('/purchase', lw_purchases.InsertNewPurchase);
app.get('/users/:id', lw_users.GetById);
app.get('/merchants/nearby', lw_merchants.GetNearbyMerchants);
app.get('/merchants/get/:id', lw_merchants.GetById);
//app.post('/users/login', lw_users.Login);
app.post('/billing', lw_billings.CreateBilling);
//app.post('/billing/charge', lw_billings.ChargeBilling);
//app.get('/billing/getbyid/:id', lw_billings.GetById)
//app.get('/billing/getbysubscriber/:id', lw_billings.GetBySubscriber)
app.get('/purchase/getbyid/:id', lw_purchases.GetById)
//app.get('/purchase/getbysubscription/:id', lw_purchases.GetBySubscription)


app.get('/testmethod', function(req, res) {
  
  res.write('\n app.addy.port ' + app.address.port);
  res.write('\n app.addy.ip ' + app.address.ip);
  res.write('\n app.addy.host ' + app.address.host);
  res.write('\n req.conn.remoteaddy ' + req.connection.remoteAddress);
  res.write('\n req.headers-x-fwd-for ' + req.headers['host']);
  res.write('\n req.user-agent ' + req.headers['user-agent']);
  res.write('\n accept-language ' + req.headers['accept-language']);
  res.write('\n accept-charset ' + req.headers['accept-charset']);
  res.write('\n req.sock.remoteaddy ' + req.socket.remoteAddress);
  res.write('\n req.host ' + req.host);
  res.write('\n req.protocol ' + req.protocol);
  res.write('\n req.ecure ' + req.secure);
  res.write('\n req.originalurl ' + req.originalUrl);
  res.write('\n ENV ' + process.env.NODE_ENV);
  res.write('\n env ' + process.env);
  res.end('done');
});
/*
app.get('/testauth', function(req, res) {
	//var queryObject = url.parse(req.url,true).query;
	
	lw_auth.ValidateRequestQS(req, res, function(result_code, result_text){
		if (result_code == 0)
		{ console.log('DO CODE');
		  res.writeHead(400, {'Content-Type': 'text/plain'});
		  res.end('Alles klar!');
		} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		}
	});
});*/

app.get('/testheader', function(req, res) {
	
	lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{ console.log('DO CODE');
		  res.writeHead(400, {'Content-Type': 'text/plain'});
		  res.write('Subscriber ID = ' + subscriber_id);
		  res.end('Alles klar!');
		} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		}
	});
});

app.listen(port, function() {
  console.log("Listening on " + port);
});


/*
	lw_auth.ValidateRequest(req, res, function(result_code, result_text, subscriber_id, plan_id){
		if (result_code == 0)
		{ DO CODE
		} 
		else
		{
			res.writeHead(401, {'Content-Type': 'text/plain'}); 
			res.write(result_text); 
			res.end('NOT AUTH');
		}
	});
*/